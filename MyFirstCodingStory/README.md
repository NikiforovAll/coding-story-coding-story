# MyFirstCodingStory

**To read**: <https://gitlab.com/NikiforovAll/coding-story-coding-story>

**Estimated reading time**: 10 min

## Story Outline

Coding story about coding story.

## Story Organization

**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #getting-started
